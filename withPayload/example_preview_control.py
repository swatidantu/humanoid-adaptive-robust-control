import numpy as np
from robot import Bipedal
import tform as tf
import time
from walking_generator import PreviewControl
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd
import json


if __name__ == "__main__":
    bipedal = Bipedal() 
    zc = 0.45 #Center of Mass height [m]
    stride = 0.1
    CoM_to_body = np.array([0.0, 0.0, 0.0]) #from CoM to body coordinate

    targetRPY = [0.0, 0.0, 0.0]
    pre = PreviewControl(Tsup_time=0.3,Tdl_time=0.1, previewStepNum=190)#preview control
    bipedal.positionInitialize(initializeTime=0.2)
    start = 0.0
    end = 48.0
    t = np.linspace(start,end,num = 4800)
    p = np.linspace(start,49.92,num = 4992)
    q = np.linspace(start,48.01,num = 4801)
    CoMTrajectory = np.empty((0,3), float)
    trjR_log = np.empty((0,3), float)
    trjL_log = np.empty((0,3), float)
    trjJoint = np.empty((0,6),float)
    trjError = np.empty((0,6),float)
    grfl = np.empty((0,1), float)
    grfr = np.empty((0,1), float)
    C = np.empty((0,1),float)
    beta0 = np.empty((0,1),float)
    beta1 = np.empty((0,1),float)
    trjTor = np.empty((0,6),float)
    erRMS = 0.0
    T = 0.0
    erM = np.empty((0,6),float)
    ersq = np.empty((0,6),float)
    RMS = np.empty((0,6),float)
    MAE = np.empty((0,6),float)
    colours = ['red',
            'brown',
            'cyan',
            'pink',
            'purple',
            'orange']
    joint_labels = ['r_yaw_hipJoint',
        	    'r_roll_hipJoint',
       	        'r_pitch_hipJoint',
                'knee',
                'r_pitch_ankleJoint',
                'r_roll_ankleJoint']
    walkingCycle = 50
    supPoint = np.array([0.,0.065])
    for w in range(walkingCycle):
        comTrj,footTrjL,footTrjR = pre.footPrintAndCOMtrajectoryGenerator(inputTargetZMP=supPoint, inputFootPrint=supPoint) #generate one cycle trajectory

        
        CoMTrajectory = np.vstack((CoMTrajectory, comTrj))
        trjR_log = np.vstack((trjR_log, footTrjR ))
        trjL_log = np.vstack((trjL_log, footTrjL )) 
        # grfr = np.vstack((grfr, a))   
        # grfl = np.vstack((grfl, b))  
        com_len = len(comTrj)
        for i in range(com_len):
            targetPositionR = footTrjR[i] - comTrj[i]
            targetPositionL = footTrjL[i] - comTrj[i]

            PosR = bipedal.inverseKinematics(targetPositionR, targetRPY, bipedal.R)
            PosL = bipedal.inverseKinematics(targetPositionL, targetRPY, bipedal.L)
            objPR = bipedal.getJointPositions(bipedal.R)
            objPL = bipedal.getJointPositions(bipedal.L)
         #   VelR = bipedal.jacobian(PosR,bipedal.R)
         #   VelL = bipedal.jacobian(PosL,bipedal.L)
            objVR = bipedal.getJointVelocities(bipedal.R)
            objVL = bipedal.getJointVelocities(bipedal.L)
            
            er = ((PosR-objPR)*30)
            # ersq = (er**2)
            # erRMS = erRMS + ersq
            # trjJoint = np.vstack((trjJoint, PosL*57.29))
            # trjError = np.vstack((trjError, er))

            torR = bipedal.adaptiveRobust(PosR,objPR,objVR,bipedal.R)
            torL = bipedal.adaptiveRobust(PosL,objPL,objVL,bipedal.L)
            trjTor = np.vstack((trjTor, torR))
            # beta0 = np.vstack((beta0, a))
            # beta1 = np.vstack((beta1, b))
            # C = np.vstack((C, c))
            # erM = np.vstack((erM, er))
            # T = T + (torR**2)
            # print(torR)
            # print(a)
            # print(b)
            # print(c)
            bipedal.setLeftLegJointPositions(PosL,torL)
            bipedal.setRightLegJointPositions(PosR,torR)

            bipedal.oneStep()

        supPoint[0] += stride
        supPoint[1] = -supPoint[1]
    # with open('COMtrajectory.txt', 'w') as outfile:
    #     json.dump(CoMTrajectory.tolist(),outfile)
    # with open('foottrajectoryR.txt', 'w') as outfile:
    #     json.dump(trjR_log.tolist(),outfile)
    # with open('foottrajectoryL.txt', 'w') as outfile:
    #     json.dump(trjL_log.tolist(),outfile)
    # with open('TorqueS1.txt', 'w') as outfile:
    #     json.dump(trjTor.tolist(),outfile)  
    # with open('CS2ATDC.txt', 'w') as outfile:
    #      json.dump((C*1.8).tolist(),outfile)  
    # with open('beta0S2Proposed.txt', 'w') as outfile:
    #     json.dump(beta0.tolist(),outfile)  
    # with open('beta1S2Proposed.txt', 'w') as outfile:
    #     json.dump(beta1.tolist(),outfile)    
    # RMS = (np.sqrt((erRMS/4800)))
    # TRMS = (np.sqrt((T/4800)))
    # abc = pd.DataFrame(erM, index = list(range(0, 4800)), columns = list('123456'))
    # MAE = abc.max()
    # print(RMS)
    # print(MAE)
    # print(TRMS)
    #print(pre.px_ref_log[:])
    #print(pre.px)
    #debug Com
    # figx = plt.figure(figsize=(10,10))
    # comx = figx.add_subplot(111)
    # comx.plot(t,CoMTrajectory[:,0],label="CoMtrj",color="blue")
    # comx.plot(p,pre.px_ref_log[:],label="targetZMP",color="black")
    # comx.plot(q,pre.px,label="ZMP",color="red")
    # plt.legend()

    # figy = plt.figure(figsize=(10,10))
    # comy = figy.add_subplot(111)
    # figy=plt.xlabel("time(s)")
    # #figy=plt.ylabel("angles(degree)")
    # comy.set_title("COM and foot Trajectory")
    # comy.plot(t,CoMTrajectory[:,0],label="CoMtrj",color="blue")
    # comy.plot(p,pre.py_ref_log[:],label="targetZMP",color="black")
    # comy.plot(q,pre.py,label="ZMP",color="red")
    # plt.legend()


    
##    figz = plt.figure(figsize=(5,2))
##    comz = figz.add_subplot(111)
##    figz=plt.xlabel("time (centi-seconds)")
##    figz=plt.ylabel("joint angles (degree)")
##    comz.set_title("Joint Trajectory")
##    comz.plot(t,trjJoint[:,0], color = colours[0],linestyle = 'solid', lw=2)
##    comz.set_ylim(-30.0, 30.0)
##    comz.legend()
##    plt.pause(0.01)
##    plt.legend() 
##
##    figa = plt.figure(figsize=(5,2))
##    coma = figa.add_subplot(111)
##    figa=plt.xlabel("time (centi-seconds)")
##    figa=plt.ylabel("joint angles (degree)")
##    coma.set_title("Joint Trajectory")
##    coma.plot(t,trjJoint[:,1], color = colours[1],linestyle = 'solid', lw=2)
##    coma.set_ylim(-30.0, 30.0)
##    coma.legend()
##    plt.pause(0.01)
##    plt.legend() 
##
##    figb = plt.figure(figsize=(5,2))
##    comb = figb.add_subplot(111)
##    figb=plt.xlabel("time (centi-seconds)")
##    figb=plt.ylabel("joint angles (degree)")
##    comb.set_title("Joint Trajectory")
##    comb.plot(t,trjJoint[:,2], color = colours[2],linestyle = 'solid', lw=2)
##    comb.set_ylim(-60.0, 0.0)
##    comb.legend()
##    plt.pause(0.01)
##    plt.legend() 
##
##    figc = plt.figure(figsize=(5,2))
##    comc = figc.add_subplot(111)
##    figc=plt.xlabel("time (centi-seconds)")
##    figc=plt.ylabel("joint angles (degree)")
##    comc.set_title("Joint Trajectory")
##    comc.plot(t,trjJoint[:,3], color = colours[3],linestyle = 'solid', lw=2)
##    comc.set_ylim(120.0, 60.0)
##    comc.legend()
##    plt.pause(0.01)
##    plt.legend() 
##
##    figd = plt.figure(figsize=(5,2))
##    comd = figd.add_subplot(111)
##    figd=plt.xlabel("time (centi-seconds)")
##    figd=plt.ylabel("joint angles (degree)")
##    comd.set_title("Joint Trajectory")
##    comd.plot(t,trjJoint[:,4], color = colours[4],linestyle = 'solid', lw=2)
##    comd.set_ylim(-75.0, 0.0)
##    comd.legend()
##    plt.pause(0.01)
##    plt.legend() 
##
#    fige = plt.figure(figsize=(5,2))
#    come = fige.add_subplot(111)
#    fige=plt.xlabel("time (centi-seconds)")
#    fige=plt.ylabel("joint angles (degree)")
#    come.set_title("Joint Trajectory")
#    come.plot(t,trjJoint[:,5], color = colours[5],linestyle = 'solid', lw=2)
#    come.set_ylim(-30.0, 30.0)
#    come.legend()
#    plt.pause(0.01)
#    plt.legend() 

    
    # figa = plt.figure(figsize=(7,3))
    # coma = figa.add_subplot(111)
    # figa=plt.xlabel("time (seconds)")
    # figa=plt.ylabel("joint angles (degree)")
    # coma.set_title("Tracking Error Proposed")
    # for i in range(len(PosR)):
    #     coma.plot(t,trjError[:,i], color = colours[i],linestyle = 'solid', lw=2, label='q'+str(i))
    # coma.set_ylim(-10.0, 20.0)
    # coma.set_xlim(0.0, 50.0)
    # coma.legend(ncol=6, loc='upper center')
        

    
    # figb = plt.figure(figsize=(7,3))
    # comb = figb.add_subplot(111)
    # figb=plt.xlabel("time (seconds)")
    # figa=plt.ylabel("joint angles (degree)")
    # comb.set_title("Torque Proposed")
    # for i in range(len(PosR)):
    #     comb.plot(t,trjTor[:,i], color = colours[i],linestyle = 'solid', lw=2, label='q'+str(i))
    # comb.set_ylim(-25.0, 40.0)
    # comb.set_xlim(0, 50.0)
    # comb.legend(ncol=6, loc='upper center') 

#     figy = plt.figure(figsize=(7,3)) 
#     comy = figy.add_subplot(111)
#     figy=plt.xlabel("time (seconds)")
#     comy.set_title("Adaptive gains (proposed)")
#     comy.plot(t,beta0,label="b0",color="blue")

#     comy.set_xlim(0, 50.0)
#     plt.legend()
# #    comy.plot(pre.py,label="ZMP",color="red")
# #    plt.legend()
#     comz = comy.twinx()
#     comz.plot(t,beta1,label="b1",color="red")

#     plt.legend(ncol=2, loc='upper center')

    # figw = plt.figure(figsize=(5,2))
    # comw = figw.add_subplot(111)
    # figw=plt.xlabel("time (s)")
    # figw=plt.ylabel("")
    # comw.set_title("Ground reaction forces(N)")
    # #for i in range(len(PosR)):
    # comw.plot(t,grfr,label="right leg", color = "red" ,linestyle = 'solid', lw=2)
    # comw.plot(t,grfl,label="left leg", color = "blue" ,linestyle = 'solid', lw=2)
    # comw.set_xlim(0.0, 48.0)
    # comw.legend(ncol=2)
    # plt.pause(0.01)
    

    # figx = plt.figure(figsize=(5,2))
    # comx = figx.add_subplot(111)
    # figx=plt.xlabel("time (seconds)")
    # figx=plt.ylabel("")
    # comx.set_title("Adaptive gains c for ARTDC proposed")
    # #for i in range(len(PosR)):
    # comx.plot(t,C/45,label="C", color = "orange" ,linestyle = 'solid', lw=2)
    # comx.set_ylim(0.0, 30.0)
    # comx.legend()
    # plt.pause(0.01)
    # plt.show()



    # foot trajectory 

    # fig = plt.figure(figsize=(10,10))
    # ax1 = Axes3D(fig)
    # ax1.set_xlabel("time(s)")
    # ax1.set_ylabel("y")
    # ax1.set_zlabel("z")

    # ax1.plot(trjL_log[:,0],trjL_log[:,1],trjL_log[:,2],label="Left_leg_traj",color="blue",linestyle='solid',lw=2)
    # ax1.plot(trjR_log[:,0],trjR_log[:,1],trjR_log[:,2],label="Right_leg_traj",color="orange",linestyle='solid',lw=2)

    # ax1.plot(CoMTrajectory[:,0],CoMTrajectory[:,1],CoMTrajectory[:,2],label="COM_traj",color="green",linestyle='solid',lw=2)
    # ax1.xaxis.set_minor_locator(AutoMinorLocator())

    # ax1.tick_params(which='both', width=2)
    # ax1.tick_params(which='major', length=7)
    # ax1.tick_params(which='minor', length=4)
    # ax1.set_title("COM and foot trajectory ")
    
    
    # plt.show()




    bipedal.disconnect()

